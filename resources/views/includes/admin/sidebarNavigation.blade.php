
<aside class="app-sidebar">
    <div class="app-sidebar__user"><img class="app-sidebar__user-avatar"
                                        src="https://s3.amazonaws.com/uifaces/faces/twitter/jsa/48.jpg"
                                        alt="User Image">
        <div>
            <p class="app-sidebar__user-name">{{Auth::user()->name}}</p>

            <p class="app-sidebar__user-designation">Full Stack Devoloper</p>
        </div>
    </div>
    <ul class="app-menu">
        <li><a class="app-menu__item active" href="{{route('dashboard')}}"><i class="app-menu__icon fa fa-dashboard"></i><span
                    class="app-menu__label">Dashboard</span></a></li>



                                    {{--  User--}}



        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i
                    class="app-menu__icon fa fa-laptop"></i><span class="app-menu__label">User</span><i
                    class="treeview-indicator fa fa-angle-right"></i></a>
            <ul class="treeview-menu">
                <li><a class="treeview-item" href="{{route('UserDashboard')}}"><i class="icon fa fa-circle-o"></i>Dashboard</a>
                </li>


            </ul>
        </li>


               @if(Auth::user()->author==true)

                                        {{--        Author--}}

        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i
                    class="app-menu__icon fa fa-laptop"></i><span class="app-menu__label">Author</span><i
                    class="treeview-indicator fa fa-angle-right"></i></a>
            <ul class="treeview-menu">
                <li><a class="treeview-item" href="{{route('authorDashboard')}}"><i class="icon fa fa-circle-o"></i>Dashboard</a>
                </li>
                <li><a class="treeview-item" href="{{route('authorallpost')}}"><i class="icon fa fa-circle-o"></i>Post</a>
                </li>
                <li><a class="treeview-item" href="{{route('authorComment')}}"><i class="icon fa fa-circle-o"></i>Comments</a>
                </li>

            </ul>
        </li>

        @endif
                                    {{--   Admin--}}


        @if(Auth::user()->admin==true)


        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i
                    class="app-menu__icon fa fa-laptop"></i><span class="app-menu__label">Admin</span><i
                    class="treeview-indicator fa fa-angle-right"></i></a>
            <ul class="treeview-menu">
                <li><a class="treeview-item" href="{{route('adminDashboard')}}"><i class="icon fa fa-circle-o"></i>Dashboard</a>
                </li>
                <li><a class="treeview-item" href="{{route('adminPost')}}"><i class="icon fa fa-circle-o"></i>Post</a>
                </li>
                <li><a class="treeview-item" href="{{route('adminComment')}}"><i class="icon fa fa-circle-o"></i>Comments</a>
                </li>
                <li><a class="treeview-item" href="{{route('adminuser')}}"><i class="icon fa fa-user"></i>User</a>
                </li>


            </ul>
        </li>
            @endif

    </ul>
</aside>


