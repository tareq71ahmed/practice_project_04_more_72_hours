@extends('layouts.admin')
@section('content')



    <main class="app-content">

        <div class="row">
            <div class="col-md-9">
                <div class="tile">
                    <h3 class="tile-title">Vertical Form</h3>
                    <div class="tile-body">



                        @if($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif


                            @if(Session::has('success'))
                                <div class="alert alert-success">
                                    {{Session::get('success')}}
                                </div>
                            @endif









                        <form action="{{route('authornewpost')}}" method="post">
                            @csrf

                            <div class="form-group">
                                <label class="control-label">Title</label>
                                <input name="title" class="form-control" type="text" placeholder="Enter Title">
                            </div>



                            <div class="form-group">
                                <label class="control-label"></label>
                                <textarea name="content" type="text" class="form-control" rows="7" placeholder="Enter your Title"></textarea>
                            </div>




                            <div class="tile-footer">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>submit</button>
                            </div>


                        </form>




                    </div>

                </div>
            </div>



</div>

    </main>











@endsection

