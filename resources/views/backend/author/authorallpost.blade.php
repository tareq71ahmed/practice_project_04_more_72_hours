@extends('layouts.admin')
@section('content')


    <main class="app-content">
        <div class="app-title">
            <div>
                <h1><i class="fa fa-th-list"></i> Basic Tables</h1>
                <p>Basic bootstrap tables</p>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
                <li class="breadcrumb-item">Tables</li>
                <li class="breadcrumb-item active"><a href="#">Simple Tables</a></li>
            </ul>
        </div>
        <div class="row">








            <div class="col-md-12">
                <div class="tile">
                    <h3 class="tile-title">All Post</h3>
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>content</th>
                            <th>Created_at</th>
                            <th>Updated_at</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>

                            @foreach(Auth::user()->Posts as $post)

                            <td>{{Auth::user()->id}}</td>
                            <td>{{$post->title}}</td>
                            <td>{{$post->content}}</td>
                                <td>{{\Carbon\Carbon::parse($post->created_at)->diffForHumans()}}</td>
                                <td>{{\Carbon\Carbon::parse($post->updated_at)->diffForHumans()}}</td>
                            <td>
                                <a href="{{route('editpost',$post->id)}}" class="btn btn-secondary">Edit</a>


                                <form id="deletePost-{{$post->id}}" method="post" action="{{route('deletePost',$post->id)}}">@csrf</form>

                                <a onclick="document.getElementById('deletePost-{{$post->id}}').submit()" href="#" class="btn btn-danger btn-sm">Delete</a>




                            </td>

                        </tr>
                            @endforeach




                        </tbody>
                    </table>
                </div>
            </div>








        </div>
    </main>








@endsection
