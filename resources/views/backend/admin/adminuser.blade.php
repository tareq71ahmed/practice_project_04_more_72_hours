
@extends('layouts.admin')
@section('content')


    <main class="app-content">
        <div class="app-title">
            <div>
                <h1><i class="fa fa-th-list"></i> Admin User</h1>
                <p>Admin User tables</p>
            </div>

        </div>



        <div class="row">








            <div class="clearfix"></div>


            <div class="col-md-12">
                <div class="tile">
                    <h3 class="tile-title">Table Hover</h3>
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach( $users as $user)
                            <tr>
                                <td>{{$user->id}}</td>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{\Carbon\Carbon::parse($user->created_at)->diffForHumans()}}</td>
                                <td>{{date_format($user->created_at,'F d,y')}}</td>

                                <td>
                                    <a href="{{route('edituser',$user->id)}}" class="btn ml-2 ">Edit</a>

                                    <form id="deleteuser-{{$user->id}}" method="post" action="{{route('deleteuser',$user->id)}}">@csrf</form>

                                    <a onclick="document.getElementById('deleteuser-{{$user->id}}').submit()" href="#" class="btn btn-danger btn-sm">Delete</a>



                                </td>



                            </tr>
                        @endforeach




                        </tbody>
                    </table>
                </div>
            </div>
            <div class="clearfix"></div>



        </div>
    </main>


@endsection


