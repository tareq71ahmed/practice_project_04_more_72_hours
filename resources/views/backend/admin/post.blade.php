
@extends('layouts.admin')
@section('content')


    <main class="app-content">
        <div class="app-title">
            <div>
                <h1><i class="fa fa-th-list"></i> Basic Tables</h1>
                <p>Basic bootstrap tables</p>
            </div>

        </div>



        <div class="row">








            <div class="clearfix"></div>


            <div class="col-md-12">
                <div class="tile">
                    <h3 class="tile-title">Table Hover</h3>
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Content</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach( $posts as $post)
                            <tr>
                                <td>{{$post->id}}</td>
                                <td>{{$post->title}}</td>
                                <td>{{$post->content}}</td>
                                <td>{{\Carbon\Carbon::parse($post->created_at)->diffForHumans()}}</td>
                                <td>{{date_format($post->created_at,'F d,y')}}</td>
                                <td>
                                    <a href="{{route('editpost',$post->id)}}" class="btn ml-2 ">Edit</a>

                                    <form id="deletePost-{{$post->id}}" method="post" action="{{route('deletePost',$post->id)}}">@csrf</form>

                                    <a onclick="document.getElementById('deletePost-{{$post->id}}').submit()" href="#" class="btn btn-danger btn-sm">Delete</a>



                                </td>



                            </tr>
                        @endforeach




                        </tbody>
                    </table>
                </div>
            </div>
            <div class="clearfix"></div>



        </div>
    </main>


@endsection

