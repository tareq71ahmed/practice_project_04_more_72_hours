


@extends('layouts.admin')
@section('content')


    <main class="app-content">


        <div class="row">
            <div class="col-md-8">
                <div class="tile">
                    <h3 class="tile-title">Create Post</h3>
                    <div class="tile-body">



                        @if($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif




                        @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{Session::get('success')}}
                            </div>
                        @endif



                            <form action="{{'updatePost',$post->id}}" method="post">
                            @csrf

                            <div class="form-group">
                                <label class="control-label">Title</label>
                                <input value="{{$post->title}}" name="title" class="form-control"  type="text" placeholder="Enter Title">
                            </div>



                            <div class="form-group">
                                <label class="control-label">Content</label>



                                <textarea  name="content"class="form-control" rows="6" placeholder="Enter your Content">{{$post->content}}</textarea>

                            </div>

                            <div class="tile-footer">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update</button>
                            </div>
                        </form>










                    </div>

                </div>
            </div>




        </div>
    </main>




@endsection

