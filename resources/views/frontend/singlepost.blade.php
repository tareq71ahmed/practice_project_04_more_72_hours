
@extends('frontend.layouts.master')
@section('content')


    <!-- Page Header -->
<header class="masthead" style="background-image:url('{{'frontend/img/contact.jpg'}}')">


    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <div class="post-heading">
                    <h1>{{$post->title}}</h1>

                    <span class="meta">Posted by
              <a href="#">{{$post->user->name}}</a>
              on {{date_format($post->created_at,'F d,y')}}</span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Post Content -->
<article>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                {{$post->content}}
            </div>
        </div>
    </div>
</article>
    <div class="comment container">
        <h6>COMMENT</h6>
        @foreach($post->comments as $comment)



            <hr>
            <p>{{$comment->content}}</p>
            <small class="text-info"><p>Posted bY {{$comment->user->name}} | {{date_format($comment->created_at,'F d,y')}}</p></small>

        @endforeach

    </div>
<hr>

    @if(Auth::check())
            <form action="{{route('newComment')}}" method="post">
    @csrf

                <div class="form-group container">
                    <textarea class="form-control" name="comment" rows="5" cols="30" placeholder="make comment.."></textarea>

                    <input type="hidden" name="post" value="{{$post->id}}">


                </div>

                <div class="form-group container">
                    <button class="btn btn-secondary" type="submit">comment</button>
                </div>

</form>

@endif

@endsection
