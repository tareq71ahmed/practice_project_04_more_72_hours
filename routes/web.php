<?php



    Route::get('/','PublicController@index')->name('index');
    Route::get('post/{id}','PublicController@singlepost')->name('singlepost');



        //--------------User---------------

    Route::prefix('user')->group(function(){

        Route::get('dashboard','UserController@userprofile')->name('UserDashboard');
        Route::post('new-comment','UserController@newComment')->name('newComment');
        Route::get('profile','UserController@userprofile')->name('userprofile');
        Route::post('profile','UserController@userprofilepost')->name('userprofilepost');




});

        //--------------author---------------

    Route::prefix('author')->group(function(){
        Route::get('dashboard','AuthorController@dashboard')->name('authorDashboard');
        Route::get('comment','AuthorController@comment')->name('authorComment');
        Route::get('post','AuthorController@authorPost')->name('authorPost');
        Route::post('post','AuthorController@authornewpost')->name('authornewpost');
        Route::get('allPost','AuthorController@authorallpost')->name('authorallpost');
        Route::get('post/{id}','AuthorController@editpost')->name('editpost');
        Route::post('post/{id}','AuthorController@updatePost')->name('updatePost');
        Route::post('posts/{id}/delete', 'AuthorController@deletePost')->name('deletePost');


    });





            //--------------admin---------------

    Route::prefix('admin')->group(function(){
            Route::get('dashboard','AdminController@adminDashboard')->name('adminDashboard');
            Route::get('post','AdminController@post')->name('adminPost');
            Route::get('comment','AdminController@comment')->name('adminComment');
            Route::get('post/{id}/editpost', 'AdminController@editpost')->name('editpost');
            Route::post('post/{id}/updatePost', 'AdminController@updatePost')->name('updatePost');
           Route::post('posts/{id}/delete', 'AdminController@deletePost')->name('deletePost');
           Route::get('user', 'AdminController@adminuser')->name('adminuser');

//             edit,update,delete
        Route::get('user/{id}/edituser', 'AdminController@edituser')->name('edituser');
        Route::post('user/{id}/updateuser', 'AdminController@updateuser')->name('updateuser');
        Route::post('user/{id}/delete', 'AdminController@deleteuser')->name('deleteuser');



});








Auth::routes();


Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');
