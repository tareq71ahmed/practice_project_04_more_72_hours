<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Post;

use Illuminate\Support\Facades\Auth;
use App\Http\Requests\authornewpost;

class AuthorController extends Controller
{

    public function __construct(){
        return $this->middleware('CheckRole:admin');
    }



    public function dashboard(){
        return view('backend.author.dashboard');
    }

    public function comment(){
        return view('backend.author.comment');
    }

    public function authorPost(){
        return view('backend.author.authorPost');
    }


 public  function  authornewpost(authornewpost $request){

        $post = new Post();
     $post->user_id=Auth::id();
     $post->title=$request['title'];
     $post->content=$request['content'];
     $post->save();
     return back()->with('success','new post updated');

 }


       public function authorallpost(){


        return view('backend.author.authorallpost');
}

  public function editpost($id){

      $post= Post::where('id',$id)->where('user_id',Auth::id())->first();
      return view('backend.author.editpost',  compact('post'));

  }


    public function updatePost(authornewpost $request, $id){
        $post= Post::where('id',$id)->where('user_id',Auth::id())->first();
        $post->title = $request['title'];
        $post->content = $request['content'];
        $post->user_id = Auth::id();
        $post->save();

        return back()->with('success','Post Updated Successfully');


    }

    public function deletePost($id){
        $post= Post::where('id',$id)->where('user_id',Auth::id())->first();
        $post->delete();
        return back();
    }







}
