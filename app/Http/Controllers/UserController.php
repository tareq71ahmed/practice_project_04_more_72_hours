<?php

namespace App\Http\Controllers;
use App\Comment;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use App\Http\Requests\profileRequest;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function dashboard(){
        return view('user.dashboard');
    }



  public function UserProfile(){
        return view('user.UserProfile');
    }



    public function userprofilepost(profileRequest $request){
        $user=Auth::user();
        $user->name=$request['name'];
        $user->email=$request['email'];

//        if($request['password'] !=""){
//            if(!(Hash::check($request['password'],Auth::user()->password))){
//                return redirect()->back()->with('error','Current Password Does Not Match');
//            }
//        }
//        if(strcmp($request['password'], $request['new_password'])==0){
//            return redirect()->back()->with('error','New Passpord Not Same As Current');
//        }
//        $validation = $request->validate([
//            'password' => 'required',
//            'new_password' => 'required|min:6|confirmed',
//        ]);
//        $user->password = bcrypt($request['new_password']);


        $user->save();

        return back();

    }


     public function  newComment(Request $request){
        $comment = new Comment();
        $comment->post_id= $request['post'];
        $comment->user_id=Auth::id();
        $comment->content=$request['comment'];
        $comment->save();

        return back();


     }









}
