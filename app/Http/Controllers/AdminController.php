<?php

namespace App\Http\Controllers;
use App\Post;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Http\Requests\updateuser;

use Illuminate\Http\Request;
use App\Http\Middleware\CheckRole;
use App\Http\Requests\authornewpost;

class AdminController extends Controller
{

    public function __construct(){
        return $this->middleware('CheckRole:admin');
    }




    public function adminDashboard(){
        return view('backend.admin.adminDashboard');
    }

    public function post(){
        $posts = Post::all();
        return view('backend.admin.post',compact('posts'));
    }
    public function comment(){
        return view('backend.admin.comment');
    }



    public  function editpost($id){


        $post= Post::where('id',$id)->first();
        return view('backend.admin.editpost',  compact('post'));

    }

    public function adminuser(){
        $users = User::all();
        return view('backend.admin.adminuser',compact('users'));
    }







    public function updatePost(authornewpost $request, $id){

        $post= Post::where('id',$id)->first();
        $post->title = $request['title'];
        $post->content = $request['content'];
        $post->save();

        return back()->with('success','Post Updated Successfully');
    }

    public function deletePost($id){
        $post= Post::where('id',$id)->first();
        $post->delete();
        return back();
    }



//     edit,update,delete


        public function edituser($id){

            $user= User::where('id',$id)->first();
            return view('backend.admin.edituser',  compact('user'));

        }


    public function updateuser(updateuser $request, $id){

        $user= User::where('id',$id)->first();
        $user->name = $request['name'];
        $user->email = $request['email'];
        if($request['author']==1){
            $user->author=true;
        }elseif($request['admin']==1){
            $user->admin=true;
        }
        $user->save();
        return back()->with('success','User Updated Successfully');
    }

    public function deleteuser($id){
        $user= User::where('id',$id)->first();
        $user->delete();
        return back();
    }








}
